package com.thisisbrent.reprecord.da;

import com.thisisbrent.reprecord.da.query.DataType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 *
 * Use the @Column annotation on the members of your bean class
 * so that the DAO can read and write it to the DB.
 * NOTE: your member needs a setter method!
 * e.g.
 *    // @Column(name = "your_column_name", type = DataType.TEXT)
 *    // private String foo;
 *    // public void setFoo(String foo) { this.foo = foo; }
 *    // public String getFoo() { return foo; }
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
	String name();
	DataType type();
}
