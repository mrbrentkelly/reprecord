package com.thisisbrent.reprecord.da;

import static com.google.common.collect.Lists.newArrayList;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.thisisbrent.reprecord.da.query.Create;
import com.thisisbrent.reprecord.da.query.DataType;
import com.thisisbrent.reprecord.da.query.Drop;
import com.thisisbrent.reprecord.da.query.Select;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 *
 */
public abstract class DataAccessObject<T extends Entity> implements DatabaseTable {
    private static final String _ID = "_id";
    private static final String _FOREIGN_HASH = "_foreign_hash";
    private static final String _FOREIGN_ID = "_foreign_id";
    private static final String Q_BY_ID = _ID + " = %s";
    private static final String Q_BY_FOREIGN = _FOREIGN_ID + " = %s and " + _FOREIGN_HASH + " = '%s'";

    private Context ctx;
    private String tag;

    public DataAccessObject() {}

    public DataAccessObject(Context ctx, String tag) {
        this.ctx = ctx;
        this.tag = tag;
    }

    protected abstract Class<T> getEntityClass();

    protected RepRecordSQLiteHelper database() {
        return RepRecordSQLiteHelper.getHelper(ctx);
    }

    public String tableName() {
        return getEntityClass().getSimpleName();
    }

    /**
     * Dynamically generates a CREATE SQL statement based on the entity class
     * @return
     */
    @Override
    public String createStatement() {
        Create create = Create.table(tableName());
        addColumns(create, Entity.class);
        addColumns(create, getEntityClass());
        return create.query();
    }

    /**
     * Dynamically generates a DROP SQL statement based on the entity class
     * @return
     */
    @Override
    public String dropStatement() {
        return Drop.table(tableName()).query();
    }

    /**
     * Fetches a particular Entity from the database for a given ID.
     * If no data exists for the given ID then null is returned.
     * @param id ID of the Entity
     * @return
     */
    public T getById(Long id) {
        List<T> results = get(String.format(Q_BY_ID, id));
        return results.isEmpty() ? null : results.get(0);
    }

    /**
     * Fetches all the Entities from the database.  If no data exists
     * then an empty list is returned.
     * @return List of entities
     */
    public List<T> get() {
        return get(null);
    }

    /**
     * Fetches all the Entities from the database that match the given 'where' clause.
     * @param where Where clause. e.g. "column_name = 'someValue'"
     * @return List of Entities
     */
    public List<T> get(String where) {
        List<T> results = newArrayList();
        LinkedHashMap<String, FieldInfo> fieldInfo = new LinkedHashMap();
        List<ForeignInfo> foreignInfo = newArrayList();

        populateFieldAndForeignInfo(fieldInfo, foreignInfo, Entity.class);
        populateFieldAndForeignInfo(fieldInfo, foreignInfo, getEntityClass());

        String query = Select.from(tableName())
                .get(fieldInfo.keySet().toArray(new String[]{}))
                .where(where)
                .query();

        database().rawQuery(query, new DefaultCursorParser(results, fieldInfo.values(), foreignInfo, this));

        return results;
    }

    /**
     * Creates or Updates a given Entity to the database
     * @param t Entity to save
     */
    public void save(Entity t) {
        save(t, true);
    }

    /**
     * Creates or Updates a given Entity to the database with the option of saving foreign members.
     * @param t Entity to save
     * @param saveChildren true will save all foreign members.  false will ignore foreign members.
     */
    public void save(Entity t, boolean saveChildren) {
        Entity entity = t;
        Long id = entity.getId();

        ContentValues cv = new ContentValues();

        populateContentValues(cv, Entity.class, t);
        populateContentValues(cv, t.getClass(), t);

        if (null == id) {
            entity.setId(database().insert(tableName(), cv));
        } else {
            database().update(tableName(), String.format(Q_BY_ID, id), null, cv);
        }

        if (saveChildren) {
            saveChildren(t);
        }
    }

    /**
     * Removes a given Entity from the database if it exists
     * @param t Entity to delete
     */
    public void delete(T t) {
        Entity entity = t;
        database().delete(tableName(), String.format(Q_BY_ID, entity.getId()), null);
        entity.setId(null);
    }

    private void saveChildren(Entity parent) {
        try {
            for (Field field : parent.getClass().getDeclaredFields()) {
                Foreign foreign = field.getAnnotation(Foreign.class);
                if (null != foreign) {
                    List<? extends Entity> foreignList = (List<? extends Entity>) getValue(field.getName(), parent);
                    if (null != foreignList) {
                        for (Entity child : foreignList) {
                            parent.adopt(child);
                            DataAccessObject<? extends Entity> foreignDao = (DataAccessObject<?>) foreign.dao().getConstructor(Context.class).newInstance(ctx);
                            foreignDao.save(child);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(tag, "error saving children", e);
        }
    }

    private void populateFieldAndForeignInfo(LinkedHashMap<String, FieldInfo> fieldInfo, List<ForeignInfo> foreignInfo, Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            Column column = field.getAnnotation(Column.class);
            if (null != column) {
                fieldInfo.put(column.name(), new FieldInfo(field.getName(), column.type()));
            }

            Foreign foreign = field.getAnnotation(Foreign.class);
            if (null != foreign) {
                foreignInfo.add(new ForeignInfo(field.getName(), foreign.dao()));
            }
        }
    }

    private void populateContentValues(ContentValues cv, Class<?> clazz, Object entity) {
        for (Field field : clazz.getDeclaredFields()) {
            Column column = field.getAnnotation(Column.class);
            if (null != column) {
                cv.put(column.name(), getStringValue(field.getName(), entity));
            }
        }
    }

    private void addColumns(Create create, Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            Column column = field.getAnnotation(Column.class);
            if (null != column) {
                create.withColumn(column.name(), column.type());
            }
        }
    }

    private String getStringValue(String fieldName, Object obj) {
        Object value = getValue(fieldName, obj);

        if (null != value) {
            return value.toString();
        }

        return null;
    }

    private Object getValue(String fieldName, Object obj) {
        try {
            return obj.getClass().getMethod("get" + capitalize(fieldName)).invoke(obj);
        } catch (Exception e) {
            Log.e(tag, "error getting value", e);
        }

        return null;
    }

    private void setValue(String fieldName, Class<?> parameterType, Object value, Object obj) {
        try {
            if (null != value) {
                obj.getClass().getMethod("set" + capitalize(fieldName), parameterType).invoke(obj, value);
            }
        } catch (Exception e) {
            Log.e(tag, "error setting value", e);
        }
    }

    private String capitalize(String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    class FieldInfo {
        public FieldInfo(String fieldName, DataType dataType) {
            this.fieldName = fieldName;
            this.dataType = dataType;
        }

        public final String fieldName;
        public final DataType dataType;
    }

    class ForeignInfo {
        public ForeignInfo(String fieldName, Class<?> daoClass) {
            this.fieldName = fieldName;
            this.daoClass = daoClass;
        }

        public final String fieldName;
        public final Class<?> daoClass;
    }

    class DefaultCursorParser extends CursorParser<T> {
        private Collection<FieldInfo> fieldInfoList;
        private List<ForeignInfo> foreignInfoList;
        private DataAccessObject<?> dao;

        public DefaultCursorParser(List<T> data, Collection<FieldInfo> fieldInfoList, List<ForeignInfo> foreignInfoList, DataAccessObject<?> dao) {
            super(data);
            this.fieldInfoList = fieldInfoList;
            this.foreignInfoList = foreignInfoList;
            this.dao = dao;
        }

        @Override
        public void parse(Cursor cursor) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                try {
                    T entity = getEntityClass().newInstance();

                    int index = 0;

                    for (FieldInfo fieldInfo : fieldInfoList) {
                        switch(fieldInfo.dataType) {
                            case INT_PRIMARY_KEY:
                                entity.setId(cursor.getLong(index));
                                break;
                            case INT_FOREIGN_KEY:
                                entity.setForeignId(cursor.getLong(index));
                                break;
                            case TEXT_FOREIGN_HASH:
                                entity.setForeignHash(cursor.getString(index));
                                break;
                            case TEXT:
                                setValue(fieldInfo.fieldName, String.class, cursor.getString(index), entity);
                                break;
                            case INT:
                                setValue(fieldInfo.fieldName, Integer.class, cursor.getInt(index), entity);
                                break;
                            case DOUBLE:
                                setValue(fieldInfo.fieldName, Double.class, cursor.getDouble(index), entity);
                                break;
                            case LONG:
                                setValue(fieldInfo.fieldName, Long.class, cursor.getLong(index), entity);
                                break;
                        }

                        index++;
                    }

                    for (ForeignInfo foreignInfo : foreignInfoList) {
                        DataAccessObject<?> foreignDao = (DataAccessObject<?>) foreignInfo.daoClass.getConstructor(Context.class).newInstance(ctx);
                        List<?> foreignEntities = foreignDao.get(String.format(Q_BY_FOREIGN, entity.getId(), dao.tableName()));
                        setValue(foreignInfo.fieldName, List.class, foreignEntities, entity);
                    }

                    getData().add(entity);
                } catch (Exception e) {
                    Log.e(tag, "error parsing data", e);
                }

                cursor.moveToNext();
            }

            cursor.close();
        }
    }
}
