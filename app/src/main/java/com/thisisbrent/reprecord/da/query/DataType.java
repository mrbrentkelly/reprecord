package com.thisisbrent.reprecord.da.query;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public enum DataType {
	TEXT("TEXT"),
	INT("INTEGER"),
	DOUBLE("REAL"),
	LONG("INTEGER"),
	INT_FOREIGN_KEY("INTEGER"),
	TEXT_FOREIGN_HASH("TEXT"),
	INT_PRIMARY_KEY("INTEGER PRIMARY KEY AUTOINCREMENT");
	
	DataType(String value) {
		this.value = value;
	}
	
	private String value;
	public String value(){return value;}
}
