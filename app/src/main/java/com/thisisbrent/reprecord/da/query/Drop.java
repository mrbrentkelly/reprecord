package com.thisisbrent.reprecord.da.query;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public class Drop extends QueryBuilder {
	private Drop(String table) {
		super(table);
	}

	public static Drop table(String table) {
		return new Drop(table);
	}
	
	@Override
	public String query() {
		return new StringBuffer().append("DROP TABLE ").append(getTable()).append(";").toString();
	}
}
