package com.thisisbrent.reprecord.da;

import java.util.List;

import android.database.Cursor;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public abstract class CursorParser<T> {
	private List<T> data;
	
	public CursorParser(List<T> data) {
		this.data = data;
	}
	
	public abstract void parse(Cursor cursor);

	public List<T> getData() {
		return data;
	}	
}
