package com.thisisbrent.reprecord.da.query;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public class Create extends QueryBuilder {
	private Map<String, DataType> columns = new LinkedHashMap<String, DataType>();
	
	private Create(String table) {
		super(table);
	}
	
	public static Create table(String tableName) {
		return new Create(tableName);
	}
	
	public Create withPrimaryKey(String name) {
		columns.put(name, DataType.INT_PRIMARY_KEY);
		return this;
	}
	
	public Create withTextColumn(String name) {
		columns.put(name, DataType.TEXT);
		return this;
	}
	
	public Create withIntColumn(String name) {
		columns.put(name, DataType.INT);
		return this;
	}
	
	public Create withDoubleColumn(String name) {
		columns.put(name, DataType.DOUBLE);
		return this;
	}
	
	public Create withColumn(String name, DataType dataType) {
		columns.put(name, dataType);
		return this;
	}
	
	public String query() {
		StringBuffer query = new StringBuffer();
		
		query.append("CREATE TABLE ");
		query.append(getTable());
		query.append(" (");
		
		Iterator<String> columnKeyIterator = columns.keySet().iterator();
		
		while(columnKeyIterator.hasNext()) {
			String columnName = columnKeyIterator.next();
			String dataType  = columns.get(columnName).value();
			
			query.append(columnName + " " + dataType);
			
			if(columnKeyIterator.hasNext()) {
				query.append(", ");
			}
		}
						
		query.append(")");

		return query.toString();
	}
}
