package com.thisisbrent.reprecord.da.query;

import com.google.common.base.Strings;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public class Select extends QueryBuilder {
	String[] columns;
	private String clause;
	
	private Select(String table) {
		super(table);
	}
	
	public static Select from(String table) {
		return new Select(table);
	}

	public Select get(String... columns) {
		this.columns = columns;
		return this;
	}
	
	public Select where(String clause) {
		this.clause = clause;
		return this;
	}
	
	@Override
	public String query() {
		String columnsStr = "*";
		
		if(null != columns && columns.length > 0) {
			columnsStr = "";
			for(int a = 0; a < columns.length; a++) {
				columnsStr += columns[a];
				
				if((a+1) < columns.length) {
					columnsStr += ", ";
				}
			}
		}
		
		String where = "";
		
		if(!Strings.isNullOrEmpty(clause)) {
			where = " WHERE " + clause;
		}
		
		return "SELECT " + columnsStr + " FROM " + getTable() + where;
	}

}
