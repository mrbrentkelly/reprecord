package com.thisisbrent.reprecord.da.o;

import android.content.Context;

import com.thisisbrent.reprecord.bean.Workout;
import com.thisisbrent.reprecord.da.DataAccessObject;

/**
 * Created by Brent Kelly on 21/05/15.
 * http://www.thisisbrent.com
 */
public class WorkoutDAO extends DataAccessObject<Workout> {
    public WorkoutDAO() {}

    public WorkoutDAO(Context ctx) {
        super(ctx, ExerciseDAO.class.getName());
    }

    @Override
    public Class<Workout> getEntityClass() {
        return Workout.class;
    }
}
