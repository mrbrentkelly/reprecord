package com.thisisbrent.reprecord.da;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.common.collect.Lists;
import com.thisisbrent.reprecord.da.o.ExerciseDAO;
import com.thisisbrent.reprecord.da.o.WorkoutDAO;

import java.util.List;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public class RepRecordSQLiteHelper extends SQLiteOpenHelper {
    private static final String TAG = RepRecordSQLiteHelper.class.getName();
    private static final String DATABASE_NAME = "RepRecordDB";
    private static final int DATABASE_VERSION = 1;
    private final List<DatabaseTable> tables = Lists.<DatabaseTable>newArrayList(new ExerciseDAO(), new WorkoutDAO());
    private SQLiteDatabase database;
    private static RepRecordSQLiteHelper helper;

    private RepRecordSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static RepRecordSQLiteHelper getHelper(Context context) {
        if (null == helper) {
            // Use the application context, which will ensure that you
            // don't accidentally leak an Activity's context.
            // See this article for more information: http://bit.ly/6LRzfx
            helper = new RepRecordSQLiteHelper(context.getApplicationContext());
        }

        return helper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    private void createTables(SQLiteDatabase db) {
        for (DatabaseTable databaseTable : tables) {
            db.execSQL(databaseTable.createStatement());
        }
    }

    private void dropTables(SQLiteDatabase db) {
        for (DatabaseTable databaseTable : tables) {
            db.execSQL(databaseTable.dropStatement());
        }
    }

    private void openDatabase() {
        database = helper.getWritableDatabase();
    }

    private void closeDatabase() {
        database.close();
    }

    public void rawQuery(String query, CursorParser<?> parser) {
        try {
            openDatabase();
            parser.parse(database.rawQuery(query, null));
        } catch (Exception e) {
            Log.e(TAG, "error while performing query " + query, e);
        } finally {
            closeDatabase();
        }
    }

    public long insert(String tableName, ContentValues contentValues) {
        try {
            openDatabase();
            return database.insert(tableName, null, contentValues);
        } catch (Exception e) {
            Log.e(TAG, "error while inserting new row into table " + tableName, e);
            return -1;
        } finally {
            closeDatabase();
        }
    }

    public void update(String tableName,
                       String whereClause,
                       String[] whereArgs,
                       ContentValues contentValues) {
        try {
            openDatabase();
            database.update(tableName, contentValues, whereClause, whereArgs);
        } catch (Exception e) {
            Log.e(TAG, "error while updating row in table " + tableName, e);
        } finally {
            closeDatabase();
        }
    }

    public int delete(String tableName,
                      String whereClause,
                      String[] whereArgs) {
        try {
            openDatabase();
            return database.delete(tableName, whereClause, whereArgs);
        } catch (Exception e) {
            Log.e(TAG, "error while inserting new row into table " + tableName, e);
            return -1;
        } finally {
            closeDatabase();
        }
    }
}
