package com.thisisbrent.reprecord.da.query;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public abstract class QueryBuilder 
{
	private String table;
	
	public QueryBuilder(String table) {
		this.table = table;
	}
	
	public abstract String query();

	protected String getTable() {
		return table;
	}
}
