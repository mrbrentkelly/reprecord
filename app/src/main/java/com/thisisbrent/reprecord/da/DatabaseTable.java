package com.thisisbrent.reprecord.da;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public interface DatabaseTable {
	String tableName();
	String createStatement();
	String dropStatement();
}
