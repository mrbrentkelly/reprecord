package com.thisisbrent.reprecord.da.o;

import android.content.Context;

import com.thisisbrent.reprecord.bean.Exercise;
import com.thisisbrent.reprecord.da.DataAccessObject;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public class ExerciseDAO extends DataAccessObject<Exercise> {
    public ExerciseDAO() {}

    public ExerciseDAO(Context ctx) {
        super(ctx, ExerciseDAO.class.getName());
    }

    @Override
    public Class<Exercise> getEntityClass() {
        return Exercise.class;
    }
}
