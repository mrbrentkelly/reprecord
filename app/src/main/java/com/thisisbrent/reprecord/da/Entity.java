package com.thisisbrent.reprecord.da;

import com.thisisbrent.reprecord.da.query.DataType;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public abstract class Entity {
	@Column(name = "_id", type = DataType.INT_PRIMARY_KEY)
	private Long id;
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	@Column(name = "_foreign_hash", type = DataType.TEXT_FOREIGN_HASH)
	private String foreignHash;
	public String getForeignHash() {return foreignHash;}
	public void setForeignHash(String foreignHash) {this.foreignHash = foreignHash;}
	
	@Column(name = "_foreign_id", type = DataType.INT_FOREIGN_KEY)
	private Long foreignId;
	public Long getForeignId() {return foreignId;}
	public void setForeignId(Long foreignId) {this.foreignId = foreignId;}
	
	public void adopt(Entity child) {
		child.setForeignHash(this.getClass().getSimpleName());
		child.setForeignId(this.getId());
	}

	@Override
	public int hashCode() {
		return null != getId() ? getId().intValue() : super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj.getClass().equals(this.getClass())
				&& null != getId()
				&& getId().equals(((Entity)obj).getId());
	}
}
