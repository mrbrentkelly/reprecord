package com.thisisbrent.reprecord.bean;

/**
 * Created by Brent Kelly on 22/05/15.
 * http://www.thisisbrent.com
 */
public enum Unit {
    KG,
    LBS;

    public static String[] stringValues() {
        Unit[] units = Unit.values();
        String[] stringValues = new String[units.length];
        for(int i = 0; i < units.length; i++) {
            stringValues[i] = units[i].name();
        }
        return stringValues;
    }
}
