package com.thisisbrent.reprecord.bean;

import com.thisisbrent.reprecord.da.Column;
import com.thisisbrent.reprecord.da.Entity;
import com.thisisbrent.reprecord.da.query.DataType;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public class Exercise extends Entity {
    public Exercise() {/* For DAO*/}

    public Exercise(String name, String weight, Unit unit) {
        this.name = name;
        this.weight = weight;
        this.unit = unit;
    }

    @Column(name = "exercise_name", type = DataType.TEXT)
    private String name;
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    @Column(name = "weight", type = DataType.TEXT)
    private String weight;
    public void setWeight(String weight) {this.weight = weight;}
    public String getWeight() {return weight;}

    @Column(name = "unit", type = DataType.TEXT)
    private Unit unit;
    // for DAO parsing...
    public void setUnit(String unit) {
        this.unit = Unit.valueOf(unit);
    }
    public void setUnit(Unit unit) {this.unit = unit;}
    public String getUnit() {return unit.name();}
}
