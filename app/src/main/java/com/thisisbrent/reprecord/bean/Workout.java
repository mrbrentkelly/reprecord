package com.thisisbrent.reprecord.bean;

import com.thisisbrent.reprecord.da.Column;
import com.thisisbrent.reprecord.da.Entity;
import com.thisisbrent.reprecord.da.Foreign;
import com.thisisbrent.reprecord.da.o.ExerciseDAO;
import com.thisisbrent.reprecord.da.query.DataType;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by Brent Kelly on 21/05/15.
 * http://www.thisisbrent.com
 */
public class Workout extends Entity {
    @Column(name = "workout_name", type = DataType.TEXT)
    private String name;
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    @Foreign(dao = ExerciseDAO.class)
    private List<Exercise> exercises = newArrayList();
    public List<Exercise> getExercises() {return exercises;}
    public void setExercises(List<Exercise> propertyValues) {this.exercises = propertyValues;}
}
