package com.thisisbrent.reprecord.logic;

import android.content.Context;

import com.thisisbrent.reprecord.bean.Exercise;
import com.thisisbrent.reprecord.bean.Unit;
import com.thisisbrent.reprecord.bean.Workout;
import com.thisisbrent.reprecord.da.o.ExerciseDAO;
import com.thisisbrent.reprecord.da.o.WorkoutDAO;

import java.lang.ref.SoftReference;
import java.util.List;

/**
 * Created by Brent Kelly on 22/05/15.
 * http://www.thisisbrent.com
 */
public class RecordKeeper {
    private final ExerciseDAO exerciseDAO;
    private WorkoutDAO workoutDAO;

    private RecordKeeper(Context ctx) {
        workoutDAO = new WorkoutDAO(ctx);
        exerciseDAO = new ExerciseDAO(ctx);
    }

    public Workout getWorkout() {
        // currently only supporting a single workout so get or create one!
        List<Workout> workouts = workoutDAO.get();
        if(workouts.isEmpty()) {
            Workout w = new Workout();
            w.setName("One workout to rule them all!");

            // set up some sample exercises so the user knows what to do
            w.getExercises().add(new Exercise("Squats", "75.5", Unit.KG));
            w.getExercises().add(new Exercise("Barbell Flat Bench", "55", Unit.KG));
            w.getExercises().add(new Exercise("Bent Over Rows", "80", Unit.LBS));

            workoutDAO.save(w);
            return w;
        } else {
            return workouts.get(0);
        }
    }

    public void saveExercise(Workout workout, Exercise exercise) {
        workout.adopt(exercise);
        exerciseDAO.save(exercise);
    }

    public void deleteExercise(Exercise exercise) {
        exerciseDAO.delete(exercise);
    }

    private static SoftReference<RecordKeeper> _recordKeeper;
    public static RecordKeeper getInstance(Context ctx) {
        if(null == _recordKeeper || null == _recordKeeper.get()) {
            _recordKeeper = new SoftReference(new RecordKeeper(ctx));
        }
        return _recordKeeper.get();
    }
}
