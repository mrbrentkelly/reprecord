package com.thisisbrent.reprecord.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.thisisbrent.reprecord.R;
import com.thisisbrent.reprecord.bean.Exercise;
import com.thisisbrent.reprecord.bean.Unit;

/**
 * Created by Brent Kelly on 23/05/15.
 * http://www.thisisbrent.com
 */
public class UpsertExerciseDialog {
    private final AlertDialog alertDialog;

    public UpsertExerciseDialog(final Activity parentActivity, final OnExerciseActionedListener onExerciseActionedListener) {
        this(parentActivity, onExerciseActionedListener, Optional.<Exercise>absent());
    }

    public UpsertExerciseDialog(final Activity parentActivity, final OnExerciseActionedListener onExerciseActionedListener, final Optional<Exercise> exercise) {
        this._onExerciseActionedListener = onExerciseActionedListener;
        AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);

        final View dialogView = parentActivity.getLayoutInflater().inflate(R.layout.upsert_exercise_dialog, null);

        DialogDataHolder exerciseData = new DialogDataHolder();
        configureDialogForUpdateIfApplicable(parentActivity, dialogView, builder, exercise, exerciseData);
        initPicker(dialogView, R.id.weight_picker, 0, 250, null, String.valueOf(exerciseData.weight));
        initPicker(dialogView, R.id.weight_decimal_picker, 0, 3, new String[]{".0", ".25", ".5", ".75"}, exerciseData.decimal);
        initPicker(dialogView, R.id.unit_picker, 0, 1, Unit.stringValues(), exerciseData.unit);

        alertDialog = builder.setView(dialogView).setPositiveButton(getString(parentActivity, R.string.save), null).create();

        setOnExerciseSaved(dialogView, parentActivity, exercise);
        configureToolbar(dialogView);
    }

    public void show() {
        alertDialog.show();
    }

    public void dismiss() {
        alertDialog.dismiss();
    }

    public boolean isVisible() {
        return alertDialog.isShowing();
    }

    private void setOnExerciseSaved(final View dialogView, final Activity parentActivity, final Optional<Exercise> exercise) {
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (null != _onExerciseActionedListener) {
                            String exerciseName = String.valueOf(((TextView) dialogView.findViewById(R.id.exercise_name)).getText());
                            if (!validateExerciseName(parentActivity, exerciseName)) {
                                return;
                            }
                            String weight = getPickerValue(dialogView, R.id.weight_picker);
                            String decimal = getPickerValue(dialogView, R.id.weight_decimal_picker);
                            String unit = getPickerValue(dialogView, R.id.unit_picker);

                            Exercise savedExercise = exercise.isPresent() ? exercise.get() : new Exercise();
                            savedExercise.setName(exerciseName);
                            savedExercise.setWeight(weight + removeZero(decimal));
                            savedExercise.setUnit(Unit.valueOf(unit));

                            _onExerciseActionedListener.onExerciseSaved(savedExercise);
                        }
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    private boolean validateExerciseName(Activity parentActivity, String exerciseName) {
        if(Strings.isNullOrEmpty(exerciseName)) {
            Toast.makeText(parentActivity.getApplicationContext(),
                    getString(parentActivity, R.string.exercise_name_validation),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void configureToolbar(View dialogView) {
        Toolbar toolbar = (Toolbar)dialogView.findViewById(R.id.exercise_dialog_toolbar);
        toolbar.inflateMenu(R.menu.upsert_exercise_dialog);
        toolbar.getMenu().getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                alertDialog.dismiss();
                return false;
            }
        });
    }

    private String getString(Activity activity, int id) {
        return activity.getResources().getString(id);
    }

    private void configureDialogForUpdateIfApplicable(final Activity parentActivity,
                                                      final View dialogView,
                                                      final AlertDialog.Builder builder,
                                                      final Optional<Exercise> exercise,
                                                      DialogDataHolder dialogData) {
        if(!exercise.isPresent()) {
            return;
        }

        ((Toolbar)dialogView.findViewById(R.id.exercise_dialog_toolbar)).setTitle(getString(parentActivity, R.string.update_exercise));
        ((EditText)dialogView.findViewById(R.id.exercise_name)).setText(exercise.get().getName());

        String[] weightSplit = exercise.get().getWeight().split("\\.");
        dialogData.weight = Integer.parseInt(weightSplit[0]);
        dialogData.decimal = "." + (weightSplit.length == 2 ? weightSplit[1] : "0");
        dialogData.unit = exercise.get().getUnit();

        builder.setNegativeButton(getString(parentActivity, R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(null != _onExerciseActionedListener) {
                    _onExerciseActionedListener.onExerciseDeleted(exercise.get());
                }
            }
        });
    }

    private String removeZero(String decimal) {
        return decimal.equals(".0") ? "" : decimal;
    }

    private String getPickerValue(View dialog, int pickerId) {
        return ((StringPicker)dialog.findViewById(pickerId)).getStringValue();
    }

    private void initPicker(View dialog, int pickerId, int min, int max, String[] values, String value) {
        StringPicker picker = (StringPicker) dialog.findViewById(pickerId);
        picker.setMinValue(min);
        picker.setMaxValue(max);
        picker.setDisplayedValues(values);
        // make dialog uneditable
        picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        picker.setStringValue(value);
    }

    private OnExerciseActionedListener _onExerciseActionedListener;
    public interface OnExerciseActionedListener {
        void onExerciseSaved(Exercise exercise);
        void onExerciseDeleted(Exercise exercise);
    }

    private class DialogDataHolder {
        public int weight;
        String decimal, unit;
    }
}
