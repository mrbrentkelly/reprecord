package com.thisisbrent.reprecord.view;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.thisisbrent.reprecord.R;
import com.thisisbrent.reprecord.bean.Exercise;

/**
 * Created by Brent Kelly on 22/05/15.
 * http://www.thisisbrent.com
 */
public class ExerciseListItemHolder extends RecyclerView.ViewHolder {
    private final Drawable originalBg;
    private final int highlightBg;
    private CardView cardView;
    private ExerciseListAdapter.OnExerciseClickedListener _onExerciseClickedListener;
    private Exercise exercise;

    public ExerciseListItemHolder(final CardView cardView, ExerciseListAdapter.OnExerciseClickedListener onExerciseClickedListener) {
        super(cardView);
        this.cardView = cardView;
        this._onExerciseClickedListener = onExerciseClickedListener;
        this.originalBg = cardView.getBackground();
        this.highlightBg = cardView.getContext().getResources().getColor(R.color.listItemHighlight);
        initListeners();
    }

    private void initListeners() {
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClicked();
            }
        });
        cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                itemClicked();
                return false;
            }
        });
    }

    private void itemClicked() {
        highlight();
        if (null != _onExerciseClickedListener) {
            _onExerciseClickedListener.onExerciseClicked(exercise);
        }
    }

    private void highlight() {
        cardView.setBackgroundColor(highlightBg);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                cardView.setBackground(originalBg);
            }
        }, 100);
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
        ((TextView)cardView.findViewById(R.id.exercise_name)).setText(exercise.getName());
        ((TextView)cardView.findViewById(R.id.weight)).setText(exercise.getWeight() + exercise.getUnit());
    }
}
