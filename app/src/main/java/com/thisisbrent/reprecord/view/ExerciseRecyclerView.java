package com.thisisbrent.reprecord.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.thisisbrent.reprecord.bean.Exercise;

import java.util.List;

/**
 * Created by Brent Kelly on 22/05/15.
 * http://www.thisisbrent.com
 */
public class ExerciseRecyclerView extends RecyclerView {
    public ExerciseRecyclerView(Context context) {
        super(context);
        init();
    }

    public ExerciseRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExerciseRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setClickable(true);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        setLayoutManager(mLayoutManager);
    }

    public void setExercises(List<Exercise> exercises, ExerciseListAdapter.OnExerciseClickedListener onExerciseClickedListener) {
        setAdapter(new ExerciseListAdapter(exercises, onExerciseClickedListener));
    }

    public void addExercise(Exercise exercise) {
        ((ExerciseListAdapter)getAdapter()).addExercise(exercise);
    }

    public void removeExercise(Exercise exercise) {
        ((ExerciseListAdapter)getAdapter()).removeExercise(exercise);
    }

    public void refresh() {
        getAdapter().notifyDataSetChanged();
    }
}
