package com.thisisbrent.reprecord.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.NumberPicker;

import com.google.common.base.Strings;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by Brent Kelly on 23/05/15.
 * http://www.thisisbrent.com
 */
public class StringPicker extends NumberPicker {
    public StringPicker(Context context) {
        super(context);
    }

    public StringPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StringPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StringPicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public String getStringValue() {
        int currentValue = getValue();
        String[] displayedValues = getDisplayedValues();
        if(null == displayedValues || displayedValues.length == 0) {
            return String.valueOf(currentValue);
        } else {
            return displayedValues[currentValue];
        }
    }

    public void setStringValue(String value) {
        if(Strings.isNullOrEmpty(value)) {
            return;
        } else if(null == getDisplayedValues()) {
            setValue(Integer.parseInt(value));
        } else {
            setValue(newArrayList(getDisplayedValues()).indexOf(value));
        }
    }
}
