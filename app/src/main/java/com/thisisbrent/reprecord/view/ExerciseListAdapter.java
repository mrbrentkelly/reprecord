package com.thisisbrent.reprecord.view;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.thisisbrent.reprecord.R;
import com.thisisbrent.reprecord.bean.Exercise;

import java.util.List;

/**
 * Created by Brent Kelly on 21/05/15.
 * http://www.thisisbrent.com
 */
public class ExerciseListAdapter extends RecyclerView.Adapter<ExerciseListItemHolder>  {
    private List<Exercise> exercises;
    private OnExerciseClickedListener _onExerciseClickedListener;

    public ExerciseListAdapter(List<Exercise> exercises, OnExerciseClickedListener onExerciseClickedListener) {
        this.exercises = exercises;
        this._onExerciseClickedListener = onExerciseClickedListener;
    }

    @Override
    public ExerciseListItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cardView = (CardView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exercise_list_item, parent, false);
        return new ExerciseListItemHolder(cardView, _onExerciseClickedListener);
    }

    @Override
    public void onBindViewHolder(ExerciseListItemHolder holder, int position) {
        holder.setExercise(exercises.get(position));
    }

    @Override
    public int getItemCount() {
        return exercises.size();
    }

    public void addExercise(Exercise exercise) {
        exercises.add(exercise);
        notifyDataSetChanged();
    }

    public void removeExercise(Exercise exercise) {
        exercises.remove(exercise);
        notifyDataSetChanged();
    }

    public interface OnExerciseClickedListener {
        void onExerciseClicked(Exercise exercise);
    }
}
