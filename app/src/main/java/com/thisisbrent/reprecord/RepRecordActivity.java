package com.thisisbrent.reprecord;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.common.base.Optional;
import com.thisisbrent.reprecord.bean.Exercise;
import com.thisisbrent.reprecord.bean.Workout;
import com.thisisbrent.reprecord.logic.RecordKeeper;
import com.thisisbrent.reprecord.view.ExerciseListAdapter;
import com.thisisbrent.reprecord.view.ExerciseRecyclerView;
import com.thisisbrent.reprecord.view.UpsertExerciseDialog;

/**
 * Created by Brent Kelly on 20/05/15.
 * http://www.thisisbrent.com
 */
public class RepRecordActivity extends Activity {
    private RecordKeeper recordKeeper;
    private Workout workout;
    private UpsertExerciseDialog exerciseDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rep_record);

        setActionBar((Toolbar) findViewById(R.id.toolbar));

        recordKeeper = RecordKeeper.getInstance(this);
        workout = recordKeeper.getWorkout();
        ((ExerciseRecyclerView)findViewById(R.id.exercise_list)).setExercises(workout.getExercises(), new ExerciseListAdapter.OnExerciseClickedListener() {
            @Override
            public void onExerciseClicked(Exercise exercise) {
            exerciseDialog = new UpsertExerciseDialog(RepRecordActivity.this, onExerciseActionedListener, Optional.of(exercise));
            exerciseDialog.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        // avoids Window Leak!
        if(exerciseDialog != null && exerciseDialog.isVisible()) {
            exerciseDialog.dismiss();
            exerciseDialog = null;
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rep_record, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.add_exercise) {
            exerciseDialog = new UpsertExerciseDialog(this, onExerciseActionedListener);
            exerciseDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private UpsertExerciseDialog.OnExerciseActionedListener onExerciseActionedListener = new UpsertExerciseDialog.OnExerciseActionedListener() {
        @Override
        public void onExerciseSaved(Exercise exercise) {
            boolean isUpdate = null != exercise.getId();
            recordKeeper.saveExercise(workout, exercise);
            ExerciseRecyclerView exerciseListView = (ExerciseRecyclerView) findViewById(R.id.exercise_list);
            if(isUpdate) {
                exerciseListView.refresh();
            } else {
                exerciseListView.addExercise(exercise);
            }
            Toast.makeText(RepRecordActivity.this, "Exercise Saved!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onExerciseDeleted(Exercise exercise) {
            ((ExerciseRecyclerView) findViewById(R.id.exercise_list)).removeExercise(exercise);
            recordKeeper.deleteExercise(exercise);
            Toast.makeText(RepRecordActivity.this, "Exercise Removed!", Toast.LENGTH_SHORT).show();
        }
    };
}
