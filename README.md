# PB! Personal Best - Android Application #

PB! helps you keep track of your milestones at the Gym in the easiest and quickest way possible!  If all you want to know and keep track of is your absolute Personal Best then PB is for you!

PB! can be downloaded from the Google Play store [here](https://play.google.com/store/apps/details?id=com.thisisbrent.reprecord)

## PB! Under the hood ##

PB! makes use of an amazing ORM Framework developed by Brent Kelly which makes it extremely easy to get an app going as you don't have to worry about setting up a SQLite DB or writing any CREATE/DROP/SELECT/INSERT/UPDATE/DELETE SQL Statements.